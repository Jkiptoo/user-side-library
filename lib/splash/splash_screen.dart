import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:page_transition/page_transition.dart';

import '../Screen/login.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splashIconSize: 550,
        duration: 4000,
        splashTransition: SplashTransition.fadeTransition,
        pageTransitionType: PageTransitionType.topToBottom,
        splash: Column(
          children: [
            Image.asset('asset/systemadmin_online_2.jpg'),
            Text(
              "KIBOMET LIB APP",
              style: TextStyle(
                  fontSize: 30,
                  color: Colors.teal[900],
                  fontWeight: FontWeight.bold),
            ),
            Text('ADMIN SIDE',
                style: TextStyle(
                    fontSize: 30,
                    color: Colors.teal[900],
                    fontWeight: FontWeight.w600))
          ],
        ),
        nextScreen: const LoginScreen());
  }
}
