import 'package:flutter/material.dart';

class AboutInformation extends StatefulWidget {
  const AboutInformation({Key? key}) : super(key: key);

  @override
  State<AboutInformation> createState() => _AboutInformationState();
}

class _AboutInformationState extends State<AboutInformation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        centerTitle: true,
        title: const Text('About Library'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              const Image(image: AssetImage('asset/images.jpeg')),
              const SizedBox(
                height: 20,
              ),
              Card(
                elevation: 10,
                shadowColor: Colors.grey,
                child: Container(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: const [
                      Text(
                          'Kibomet library is a community based library established in the year two thousand and nineteen.This was after long consultation and intensive research within the kibomet community on the positive impacts and the major role the library would play.Apart from that the library was found to greatly contribute to boosting the academic capabilities of the learners in the society.'),
                      SizedBox(
                        height: 10,
                      ),
                      Image(image: AssetImage('asset/kibomet 6.jpg')),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                          'Currently we have six branches all widely spreed in the various wards examples of such we have Bidii library which is located in Hospital ward opposite Bidii community mosque. Others are in Kiminini ward,Sirende ward ,waitaluk ward and some other major locations in Kitale town respectively.'),
                      SizedBox(
                        height: 10,
                      ),
                      Image(image: AssetImage('asset/kibomet2.jpeg')),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                          'Over the past few years we have greatly partnered with learning institutions within the wards in order to reach the needs of the various categories of students based on their speciality and needs ,not forgetting all the members of '
                              'the society widely spread in the constituency.various '
                              'institutions that we have partnered with include kitale national polytechnic,AIC kibomet sec ,Makunga sec and many more some of which we are in consultations on how to effectively work together. '),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedLabelStyle: TextStyle(color: Colors.green[900]),
        unselectedLabelStyle: TextStyle(color: Colors.teal[900]),
        items: const [
          BottomNavigationBarItem(
              label: 'kibomet@gmail.com', icon: Icon(Icons.email_outlined)),
          BottomNavigationBarItem(
              label: '+254743456743',
              icon: Icon(
                Icons.phone,
              )),
        ],
      ),
    );
  }
}
