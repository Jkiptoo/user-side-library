import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class BooksInLibrary extends StatefulWidget {
  const BooksInLibrary({Key? key}) : super(key: key);

  @override
  State<BooksInLibrary> createState() => _BooksInLibraryState();
}

class _BooksInLibraryState extends State<BooksInLibrary> {
  final TextEditingController searchController = TextEditingController();
  QuerySnapshot? snapshotData;
  final database = FirebaseFirestore.instance;
  String? searchString;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    searchController.addListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Search'),
        centerTitle: true,
        backgroundColor: Colors.teal[900],
      ),
      body: Column(
        children: [
          Expanded(
              child: Column(
            children: [
              Container(
                margin: const EdgeInsets.all(20),
                child: TextFormField(
                  onChanged: (val) {
                    setState(() {
                      searchString = val.toLowerCase();
                    });
                  },
                  controller: searchController,
                  decoration: InputDecoration(
                      hintText: 'Search Books Here',
                      suffixIcon: IconButton(
                          onPressed: () {
                            searchController.clear();
                          },
                          icon: const Icon(Icons.clear)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ),
              Expanded(
                  child: StreamBuilder<QuerySnapshot>(
                stream: (searchString == null || searchString?.trim() == '')
                    ? FirebaseFirestore.instance
                        .collection('bookCollection')
                        .snapshots()
                    : FirebaseFirestore.instance
                        .collection('bookCollection')
                        .orderBy('title', descending: true)
                        .where('searchIndex', arrayContains: searchString)
                        .snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Text('We have an Error ${snapshot.error}');
                  }
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return const SizedBox(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    case ConnectionState.none:
                      return const Text('Oops No Data Found');
                    case ConnectionState.active:
                      // TODO: Handle this case.
                      break;
                    case ConnectionState.done:
                      // TODO: Handle this case.
                      return const Text('We are Done!');
                  }
                  return ListView.builder(
                      itemCount: snapshot.data?.docs.length,
                      itemBuilder: (context, index) {
                        DocumentSnapshot doc = snapshot.data!.docs[index];
                        return ListTile(
                          title: Text(doc['title']),
                        );
                      });
                },
              ))
            ],
          )),
        ],
      ),
    );
  }
}
