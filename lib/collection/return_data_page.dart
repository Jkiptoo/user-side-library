import 'package:admin/Services/return_book.dart';
import 'package:admin/dash/admin_dashboard.dart';
import 'package:admin/data/update_return.dart';
import 'package:admin/lost/lost_book.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ReturnedBookData extends StatefulWidget {
  const ReturnedBookData({Key? key}) : super(key: key);

  @override
  State<ReturnedBookData> createState() => _ReturnedBookDataState();
}

class _ReturnedBookDataState extends State<ReturnedBookData> {
  CollectionReference returnedBook =
      FirebaseFirestore.instance.collection('BookData');
  Future<void> deleteReturned(id) async {
    // ignore: avoid_print
    return returnedBook
        .doc(id)
        .delete()
        // ignore: avoid_print
        .then((value)  {
              // ignore: avoid_print
              print('Returned Book Deleted');
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Returned Book Successfully Deleted')));
            })
        .catchError((error) {
      // ignore: avoid_print
      print('Something went wrong: $error');
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Something went Wrong')));
    });
  }

  CollectionReference  lostData = FirebaseFirestore.instance.collection('Lost');
  Future<void> lostBookData(id, title, author, dateOfReturn,)async{
    return lostData.doc(id).update({
      'title': title,
      'author': author,
      'dateOfReturn': dateOfReturn,
    }).then((value) {});
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=> AdminDashboard()));
          },icon: const Icon(Icons.arrow_back),),
          centerTitle: true,
          title: const Text('Returned Book Data'),
          backgroundColor: Colors.teal[900],
        ),
        body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance.collection('BookData').snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              const  Center(
                child: CircularProgressIndicator(),
              );
            }
            final List storeReturnedBooks = [];
            snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
              Map b = documentSnapshot.data() as Map<String, dynamic>;
              storeReturnedBooks.add(b);
              b['id'] = documentSnapshot.id;
            }).toList();
            return SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    children: [
                      for (var i = 0; i < storeReturnedBooks.length; i++) ...[
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text('User:'),
                                Text('Title:'),
                                Text('Author:'),
                                Text('Code:'),
                                Text('Date:'),
                                SizedBox(height: 20,)
                              ],
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(storeReturnedBooks[i]['user']),
                                Text(storeReturnedBooks[i]['title']),
                                Text(storeReturnedBooks[i]['author']),
                                Text(storeReturnedBooks[i]['bookCode']),
                                Text(storeReturnedBooks[i]['dateOfReturn']),
                                const SizedBox(height: 20,)
                              ],
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            IconButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => UpdateReturn(
                                              id: storeReturnedBooks[i]
                                                  ['id'])));
                                },
                                icon: const Icon(
                                  Icons.edit_note,
                                  size: 50,
                              )),
                            const SizedBox(
                              width: 30,
                            ),
                            IconButton(
                                onPressed: () {
                                  deleteReturned(storeReturnedBooks[i]['id']);
                                },
                                icon: const Icon(
                                  Icons.delete,
                                  size: 50,
                                )),
                            const SizedBox(width: 30,),
                            ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 20,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  backgroundColor: Colors.black
                                ),
                                onPressed: (){
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LostBook(
                                              id: storeReturnedBooks[i]
                                              ['id'])));
                                }, child: const Text('LOST',
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 17
                            ),))
                          ],
                        )
                      ],
                      const SizedBox(
                        height: 20,
                      )
                    ],
                  )),
            );
          },
        ),
    floatingActionButton: FloatingActionButton(
      onPressed: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>  const ReturnBook()));
      },
      backgroundColor: Colors.teal[900],
      child: const Icon(Icons.add, size: 30,),
    ),
    );
  }
}
