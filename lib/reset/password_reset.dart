import 'package:admin/Screen/login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class PasswordReset extends StatefulWidget {
  const PasswordReset({Key? key}) : super(key: key);

  @override
  State<PasswordReset> createState() => _PasswordResetState();
}

class _PasswordResetState extends State<PasswordReset> {
  late String _email;
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Back'),
        centerTitle: true,
        backgroundColor: Colors.teal[900],
        elevation: 20,
      ),
      body: Column(
        children: [
          Row(
            children: [
              Padding(padding:const EdgeInsets.only(left: 20, top: 60, bottom: 5),
              child: Text('Reset password', style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.bold, color: Colors.grey[500],
              ),),)
            ],
          ),
          Column(
            children:   [
              Padding(padding: const EdgeInsets.only(left: 20, bottom: 5, right: 10),
                child: Text("Forgot your password? That's okay. It happens to everyone! Please provide your email to reset your password.",
                style: TextStyle(fontSize: 20, fontStyle: FontStyle.normal, color: Colors.grey[600]),
                ),
              )
            ],
          ),
          Container(
            padding: const EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: TextFormField(
                onChanged: (val) {
                  _email = val;
                },
                controller: _emailController,
                validator: (value) {
                  if (value!.isEmpty ||
                      !RegExp(r'^[\w-]+@([\w-]+\.)+[\w-]{2,4}')
                          .hasMatch(value)) {
                    return 'Enter a valid Email.';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  prefixIcon:const Icon(Icons.email_outlined),
                    labelText: 'Email',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20))),
              ),
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.teal[900],

            ),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  FirebaseAuth.instance
                      .sendPasswordResetEmail(email: _email)
                      .then((value) => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginScreen()))).catchError((err){
                                showDialog(context: context, builder: (BuildContext context){
                                  return AlertDialog(
                                    title: const Text('Error'),
                                    content: Text('$err'),
                                    actions: [
                                      ElevatedButton(onPressed: (){
                                        Navigator.of(context).pop();
                                      }, child: const Text('Ok', style: TextStyle(
                                        fontSize: 20,
                                      ),))
                                    ],
                                  );
                                });
                                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                    content: Text('Check email address for link')));
                  });
                }
              },
              child: const Text('Reset',style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold
              ), ))
        ],
      ),
    );
  }
}
