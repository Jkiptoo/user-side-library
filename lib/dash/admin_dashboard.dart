// ignore_for_file: must_be_immutable
import 'dart:io';
import 'package:admin/Screen/login.dart';
import 'package:admin/Services/add_book.dart';
import 'package:admin/Services/issue_book.dart';
import 'package:admin/Services/return_book.dart';
import 'package:admin/collection/about.dart';
import 'package:admin/collection/book_library.dart';
import 'package:admin/lost/retrieve_lost.dart';
import 'package:admin/splash/splash_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Services/latest_books.dart';

class AdminDashboard extends StatefulWidget {
  String? userId;
  AdminDashboard({
    Key? key,
    this.userId,
  }) : super(key: key);

  @override
  State<AdminDashboard> createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  SharedPreferences? adminData;
  String? adminEmail;
  File? _image;
  final imagePicker = ImagePicker();
  String? downloadUrl;
  Future imagePickerMethod() async {
    final pick =
        // ignore: invalid_use_of_visible_for_testing_member
        await ImagePicker.platform.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pick != null) {
        _image = File(pick.path);
      } else {
        showSnackBar('No File Selected', const Duration(milliseconds: 400));
      }
    });
  }

  var updateCount =
      FirebaseFirestore.instance.collection('Number').get().then((value) => {
            // ignore: avoid_print
            print(value.docs.length)
          });

  Future uploadImage() async {
    final postID = DateTime.now().millisecondsSinceEpoch.toString();
    Reference ref = FirebaseStorage.instance
        .ref()
        .child('${widget.userId}user_image')
        .child('post_$postID');
    await ref.putFile(_image!);
    downloadUrl = await ref.getDownloadURL();
    // ignore: avoid_print
    print(downloadUrl);
  }

  showSnackBar(String snackText, Duration d) {
    final snackBar = SnackBar(
      content: Text(snackText),
      duration: d,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();
    countDocuments();
    booksOutCounted();
    lostBookCount();
    initial();
  }

  void initial() async {
    adminData = await SharedPreferences.getInstance();
    setState(() {
      adminEmail = adminData!.getString('adminEmail');
    });
  }

  int lostCount = 0;

  lostBookCount() async {
    QuerySnapshot lostBook =
        await FirebaseFirestore.instance.collection('Lost').get();
    List<DocumentSnapshot> lostBook2 = lostBook.docs;
    lostCount = lostBook2.length;
    setState(() {
      lostCount = lostBook2.length;
    });
  }

  int booksOutCount = 0;
  booksOutCounted() async {
    QuerySnapshot booksOut =
        await FirebaseFirestore.instance.collection('BooksOut').get();
    List<DocumentSnapshot> booksOut2 = booksOut.docs;
    booksOutCount = booksOut2.length;
    setState(() {
      booksOutCount = booksOut2.length;
    });
  }

  int placeCount = 0;
  countDocuments() async {
    QuerySnapshot myDoc2 =
        await FirebaseFirestore.instance.collection('bookCollection').get();
    List<DocumentSnapshot> myDocCount2 = myDoc2.docs;
    placeCount = myDocCount2.length;
    setState(() {
      placeCount = myDocCount2.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Admin DashBoard'),
        centerTitle: true,
        backgroundColor: Colors.teal[800],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Stack(
              children: [
                Column(
                  children: <Widget>[
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: <Widget>[
                          const SizedBox(
                            width: 25,
                          ),
                          Material(
                            child: InkWell(
                              onTap: () {},
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    '$placeCount',
                                    style: const TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const Text('All Books',
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.pinkAccent))
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 50,
                          ),
                          Material(
                            child: InkWell(
                              onTap: () {},
                              child: Column(
                                children: <Widget>[
                                  Text('$booksOutCount',
                                      style: const TextStyle(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold)),
                                  const Text('Books Out',
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 50,
                          ),
                          Material(
                            child: InkWell(
                              onTap: () {},
                              child: Column(
                                children: <Widget>[
                                  Text('$lostCount',
                                      style: const TextStyle(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold)),
                                  const Text('Lost Books',
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.green))
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Image(
                        fit: BoxFit.fitWidth,
                        image: AssetImage('asset/dashboard.png'))
                  ],
                ),
              ],
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                Text(
                  'BOOKS OUT',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.teal[900],
                      fontSize: 20),
                )
              ],
            ),
            SingleChildScrollView(
              child: Column(
                children: [
                  StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection('BooksOut')
                          .snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        return (snapshot.connectionState ==
                                ConnectionState.waiting)
                            ? const Center(
                                child: CircularProgressIndicator(),
                              )
                            : ListView.builder(
                                shrinkWrap: true,
                                itemCount: snapshot.data!.docs.length,
                                itemBuilder: (context, index) {
                                  DocumentSnapshot doc =
                                      snapshot.data!.docs[index];
                                  return SingleChildScrollView(
                                    child: Card(
                                      elevation: 50,
                                      child: ListTile(
                                        title: Text(doc['title']),
                                        subtitle: Text(doc['user']),
                                        trailing: Text(doc['dateOfReturn']),
                                      ),
                                    ),
                                  );
                                });
                      }),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            label: '',
            icon: Material(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginScreen()));
                },
                child: const Image(
                    height: 30, image: AssetImage('asset/home.png')),
              ),
            )),
        BottomNavigationBarItem(
            label: '',
            icon: Material(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const BooksInLibrary()));
                },
                child: const Image(
                    height: 30, image: AssetImage('asset/library.png')),
              ),
            )),
        BottomNavigationBarItem(
          label: '',
          icon: Material(
            child: InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const AddBook()));
              },
              child:
                  const Image(height: 30, image: AssetImage('asset/add.png')),
            ),
          ),
        ),
      ]),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: CircleAvatar(
                          radius: 55,
                          child: ClipOval(
                            child: (_image != null)
                                ? Image.file(
                                    _image!,
                                    fit: BoxFit.fill,
                                    matchTextDirection: true,
                                    width: 200,
                                  )
                                : Image.asset('asset/extra.jpg'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 60),
                        child: IconButton(
                            onPressed: () {
                              imagePickerMethod();
                              uploadImage();
                            },
                            icon: const Icon(
                              Icons.camera_alt_outlined,
                              size: 30,
                            )),
                      )
                    ],
                  ),
                  Text(
                    '$adminEmail',
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            ListTile(
              title: const Text('Book Library'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const BooksInLibrary()));
              },
            ),
            ListTile(
                title: GestureDetector(
              child: const Text('Add Book'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const AddBook()));
              },
            )),
            ListTile(
              title: const Text('Issue Book'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const IssueBook()));
              },
            ),
            ListTile(
              title: const Text('Return Book'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ReturnBook()));
              },
            ),
            ListTile(
              title: const Text('Lost Book'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const RetrieveLostBooks()));
              },
            ),
            ListTile(
              title: const Text('Latest Books'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const LatestBook()));
              },
            ),
            ListTile(
              title: const Text('About'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const AboutInformation()));
              },
            ),
            ListTile(
              title: const Text('Log Out'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const SplashScreen()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
