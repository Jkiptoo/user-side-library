import 'package:admin/Services/issue_book.dart';
import 'package:admin/dash/admin_dashboard.dart';
import 'package:admin/data/return_book.dart';
import 'package:admin/data/update_issued.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class IssueBookPage extends StatefulWidget {
  const IssueBookPage({Key? key}) : super(key: key);

  @override
  State<IssueBookPage> createState() => _IssueBookPageState();
}

class _IssueBookPageState extends State<IssueBookPage> {
  final Stream<QuerySnapshot> issuedBooks =
      FirebaseFirestore.instance.collection('BooksOut').snapshots();
  CollectionReference issueBook =
      FirebaseFirestore.instance.collection('BooksOut');
  Future deleteBook(id) async {
    return await issueBook.doc(id).delete().then((value) =>
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Issued book deleted'))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            size: 25,
          ),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AdminDashboard()));
          },
        ),
        backgroundColor: Colors.teal[900],
        centerTitle: true,
        title: const Text('ISSUED BOOK DATA'),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: issuedBooks,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          final List storeBorrowedBooks = [];
          snapshot.data!.docs.map((DocumentSnapshot documentSnapshot) {
            Map a = documentSnapshot.data() as Map<String, dynamic>;
            storeBorrowedBooks.add(a);
            a['id'] = documentSnapshot.id;
          }).toList();
          return SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(15),
              child: Column(
                children: [
                  for (var i = 0; i < storeBorrowedBooks.length; i++) ...[
                   SingleChildScrollView(
                     scrollDirection: Axis.horizontal,
                     child:  Row(
                       children: [
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: const [
                             Text('User'),
                             Text('Issue'),
                             Text('Return'),
                             Text('Title'),
                             Text('Author'),
                             Text('Code'),
                             SizedBox(
                               height: 20,
                             )
                           ],
                         ),
                         const SizedBox(
                           width: 30,
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             Text(storeBorrowedBooks[i]['user']),
                             Text(storeBorrowedBooks[i]['dateOfIssue']),
                             Text(storeBorrowedBooks[i]['dateOfReturn']),
                             Text(storeBorrowedBooks[i]['title']),
                             Text(storeBorrowedBooks[i]['author']),
                             Text(storeBorrowedBooks[i]['bookCode']),
                             const SizedBox(
                               height: 20,
                             )
                           ],
                         ),
                         const SizedBox(
                           width: 10,
                         ),
                         IconButton(
                             onPressed: () {
                               Navigator.push(
                                   context,
                                   MaterialPageRoute(
                                       builder: (context) => ReturnPage(
                                           id: storeBorrowedBooks[i]['id'])));
                             },
                             icon: const Icon(
                               Icons.remove,
                               size: 50,
                             )),
                         const SizedBox(
                           width: 30,
                         ),
                         IconButton(
                             onPressed: () {
                               Navigator.push(
                                   context,
                                   MaterialPageRoute(
                                       builder: (context) => UpdateIssued(
                                           id: storeBorrowedBooks[i]['id'])));
                             },
                             icon: const Icon(
                               Icons.edit_note,
                               size: 50,
                             )),
                         const SizedBox(
                           width: 30,
                         ),
                         IconButton(
                             onPressed: () {
                               deleteBook(storeBorrowedBooks[i]['id']);
                             },
                             icon: const Icon(
                               Icons.delete,
                               size: 50,
                             ))
                       ],
                     ),
                   )
                  ]
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal[900],
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const IssueBook()));
        },
        child: const Icon(Icons.add, size: 40),
      ),
    );
  }
}
