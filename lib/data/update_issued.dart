import 'package:admin/data/issue_book_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UpdateIssued extends StatefulWidget {
  final String id;
  const UpdateIssued({Key? key, required this.id}) : super(key: key);

  @override
  State<UpdateIssued> createState() => _UpdateIssuedState();
}

class _UpdateIssuedState extends State<UpdateIssued> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference issuedUpdate =
      FirebaseFirestore.instance.collection('BooksOut');
  Future<void> updateIssuedBook(
      id, user, dateOfIssue, dateOfReturn, title, author, bookCode) async {
    issuedUpdate
        .doc(id)
        .update({
          'user': user,
          'dateOfIssue': dateOfIssue,
          'dateOfReturn': dateOfReturn,
          'title': title,
          'author': author,
          'bookCode': bookCode,
        })
        .then((value) => ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Issued Book Updated'))))
        .catchError((error) {
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text('Something went wrong: $error')));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        centerTitle: true,
        title: const Text('UPDATE ISSUED DATA'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            CircleAvatar(
              backgroundColor: Colors.teal[900],
              radius: 50,
              child: const Icon(
                Icons.edit,
                size: 40,
              ),
            ),
            Form(
              key: _formKey,
              child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                future: FirebaseFirestore.instance
                    .collection('BooksOut')
                    .doc(widget.id)
                    .get(),
                builder: (_, snapshot) {
                  if (snapshot.hasError) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Something went wrong.')));
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  var data = snapshot.data!.data();
                  var user = data!['user'];
                  var dateOfIssue = data['dateOfIssue'];
                  var dateOfReturn = data['dateOfReturn'];
                  var title = data['title'];
                  var author = data['author'];
                  var bookCode = data['bookCode'];
                  return Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(15),
                        child: TextFormField(
                          onChanged: (val) {
                            user = val;
                          },
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-zA-Z]+$').hasMatch(value)) {
                              return 'Enter a valid User';
                            }
                            return null;
                          },
                          initialValue: user,
                          autofocus: false,
                          decoration: InputDecoration(
                              labelText: 'User',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(1))),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(15),
                        child: TextFormField(
                          onChanged: (val) {
                            dateOfIssue = val;
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Date Of Issue is needed';
                            }
                            return null;
                          },
                          initialValue: dateOfIssue,
                          autofocus: false,
                          decoration: InputDecoration(
                              labelText: 'Date Of Issue',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(1))),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(15),
                        child: TextFormField(
                          onChanged: (val) {
                            dateOfReturn = val;
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Date of Return is needed';
                            }
                            return null;
                          },
                          initialValue: dateOfReturn,
                          autofocus: false,
                          decoration: InputDecoration(
                              labelText: 'Date Of Return',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(1))),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(15),
                        child: TextFormField(
                          onChanged: (val) {
                            title = val;
                          },
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return ' Please Enter a valid Title';
                            }
                            return null;
                          },
                          initialValue: title,
                          autofocus: false,
                          decoration: InputDecoration(
                              labelText: 'Title',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(1))),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(15),
                        child: TextFormField(
                          onChanged: (val) {
                            author = val;
                          },
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return ' Please Enter a valid Author';
                            }
                            return null;
                          },
                          initialValue: author,
                          autofocus: false,
                          decoration: InputDecoration(
                              labelText: 'Author',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(1))),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(15),
                        child: TextFormField(
                          onChanged: (val) {
                            bookCode = val;
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Book Code is needed';
                            }
                            return null;
                          },
                          initialValue: bookCode,
                          autofocus: false,
                          decoration: InputDecoration(
                              labelText: 'Book Code',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(1))),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.teal[900],
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  updateIssuedBook(widget.id, user, dateOfIssue,
                                          dateOfReturn, title, author, bookCode)
                                      .then((value) => Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const IssueBookPage())));
                                }
                              },
                              child: const Text('UPDATE'))
                        ],
                      )
                    ],
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
