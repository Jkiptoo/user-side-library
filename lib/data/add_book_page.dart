import 'package:admin/dash/admin_dashboard.dart';
import 'package:admin/data/update_book.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../Services/add_book.dart';

class FetchBooksData extends StatefulWidget {
  const FetchBooksData({Key? key}) : super(key: key);

  @override
  State<FetchBooksData> createState() => _FetchBooksDataState();
}

class _FetchBooksDataState extends State<FetchBooksData> {
  CollectionReference fetchBook =
      FirebaseFirestore.instance.collection('books');
  Future<void> deleteBook(id) async {
    fetchBook
        .doc(id)
        .delete()
        .then((value) => ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Book deleted successfully'))))
        .catchError((error) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Something went Wrong: $error')));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AdminDashboard()));
            },
            icon: const Icon(Icons.arrow_back)),
        title: const Text('Book Collection'),
        centerTitle: true,
        backgroundColor: Colors.teal[900],
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance.collection('books').snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return const Text('Something went Wrong');
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              const CircularProgressIndicator();
            }
            final List storeFetchBooks = [];
            snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
              Map c = documentSnapshot.data() as Map<String, dynamic>;
              storeFetchBooks.add(c);
              c['id'] = documentSnapshot.id;
            }).toList();
            return ListView(children: [
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  width: 650,
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      for (var i = 0; i < storeFetchBooks.length; i++) ...[
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text('Title:'),
                                Text('Author:'),
                                Text('Category:'),
                                Text('Description:'),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(storeFetchBooks[i]['title']),
                                Text(storeFetchBooks[i]['author']),
                                Text(storeFetchBooks[i]['category']),
                                Text(storeFetchBooks[i]['description']),
                                const SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                IconButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => UpdateBook(
                                                  id: storeFetchBooks[i]
                                                      ['id'])));
                                    },
                                    icon: const Icon(
                                      Icons.edit_note,
                                      size: 50,
                                    ))
                              ],
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                IconButton(
                                    onPressed: () {
                                      deleteBook(storeFetchBooks[i]['id']);
                                    },
                                    icon: const Icon(
                                      Icons.delete,
                                      size: 50,
                                    ))
                              ],
                            )
                          ],
                        )
                      ]
                    ],
                  ),
                ),
              ),
            ]);
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const AddBook()));
        },
        backgroundColor: Colors.teal[900],
        child: const Icon(
          Icons.add,
          size: 30,
        ),
      ),
    );
  }
}
