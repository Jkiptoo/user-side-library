import 'package:admin/data/add_book_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UpdateBook extends StatefulWidget {
  final String id;
  const UpdateBook({Key? key, required this.id}) : super(key: key);

  @override
  State<UpdateBook> createState() => _UpdateBookState();
}

class _UpdateBookState extends State<UpdateBook> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference update = FirebaseFirestore.instance.collection('books');
  Future<void> bookUpdate(id, title, author, category, description) async {
    update.doc(id).update({
      'title': title,
      'author': author,
      'category': category,
      'description': description
    }).then((value) => ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Book Successfully Updated'))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        centerTitle: true,
        title: const Text('UPDATE BOOK'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('books')
                      .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (!snapshot.hasData) {
                      return const Text('Something Went Wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    var data = snapshot.data!.data();
                    var title = data!['title'];
                    var author = data['author'];
                    var category = data['category'];
                    var description = data['description'];
                    return Column(
                      children: [
                        TextFormField(
                          onChanged: (val) {
                            title = val;
                          },
                          initialValue: title,
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return 'Please Enter a valid Title';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Title', border: OutlineInputBorder()),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          onChanged: (val) {
                            author = val;
                          },
                          initialValue: author,
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return 'Please Enter a valid Author';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Author',
                              border: OutlineInputBorder()),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          onChanged: (val) {
                            category = val;
                          },
                          initialValue: category,
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return 'Please Enter a valid Category';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Category',
                              border: OutlineInputBorder()),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          onChanged: (val) {
                            description = val;
                          },
                          initialValue: description,
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return 'Please Enter a valid Description';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Description',
                              border: OutlineInputBorder()),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.teal[900],
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10))),
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    bookUpdate(widget.id, title, author,
                                            category, description)
                                        .then((value) => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const FetchBooksData())));
                                  }
                                },
                                child: const Text('Update'))
                          ],
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal[900],
        onPressed: () {},
        child: const Icon(
          Icons.done_all,
          size: 40,
        ),
      ),
    );
  }
}
