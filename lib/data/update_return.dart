 import 'package:admin/collection/return_data_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';

class UpdateReturn extends StatefulWidget {
  final String id;
  const UpdateReturn({Key? key, required this.id}) : super(key: key);

  @override
  State<UpdateReturn> createState() => _UpdateReturnState();
}

class _UpdateReturnState extends State<UpdateReturn> {
  final DateTime _time = DateTime.now();
  final DateFormat _dateFormat = DateFormat('MMM dd yyyy');
  _handleDate() async {
    final DateTime? date = await showDatePicker(
        context: context,
        initialDate: _time,
        firstDate: DateTime(2022),
        lastDate: DateTime(2040));
    if (date != null && date != _time) {
      setState(() {
        date;
      });
      _dateController.text = _dateFormat.format(date);
    }
  }
final _dateController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  CollectionReference returned =
      FirebaseFirestore.instance.collection('BookData');

  Future<void> updateReturned(
      id, bookTitle, bookAuthor, bookCode, user, dateOfReturn) async {
    return returned
        .doc(id)
        .update({
          'bookTitle': bookTitle,
          'bookAuthor': bookAuthor,
          'bookCode': bookCode,
          'user': user,
          'dateOfReturn': dateOfReturn,
        }
            // ignore: avoid_print
            )
        // ignore: avoid_print
        .then((value) => print('Successfully Edited the book')).
    catchError((error){
      // ignore: avoid_print
      print('Something went wrong: $error');
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Something went wrong $error')));
    })
    ;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Update Returned Books'),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Card(
              child: Column(
                children: [
                  const CircleAvatar(
                    radius: 65,
                    child: Icon(
                      Icons.edit_note,
                      size: 50,
                    ),
                  ),
                  Form(
                      key: _formKey,
                      child:
                          FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                        future: FirebaseFirestore.instance
                            .collection('BookData')
                            .doc(widget.id)
                            .get(),
                        builder: (_, snapshot) {
                          if (!snapshot.hasData) {
                            return const Text('Something went wrong');
                          }
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return const CircularProgressIndicator();
                          }
                          var data = snapshot.data!.data();
                          var user = data!['user'];
                          var bookTitle = data['bookTitle'];
                          var bookAuthor = data['bookAuthor'];
                          var bookCode = data['bookCode'];
                          var dateOfReturn = data['dateOfReturn'];

                          return Padding(
                            padding: const EdgeInsets.all(0),
                            child: Column(
                              children: [
                                TextFormField(
                                  validator: (value) {
                                    // ignore: unnecessary_null_comparison
                                    if (value!.isEmpty ||
                                        // ignore: unnecessary_null_comparison
                                        value == null &&
                                            !RegExp(r'^[a-zA-Z]+$')
                                                .hasMatch(value)) {
                                      return 'Please enter a valid User';
                                    }
                                    return null;
                                  },
                                  initialValue: user,
                                  onChanged: (val) {
                                    user = val;
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'User',
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                                const SizedBox(height: 20,),
                                TextFormField(
                                  validator: (value) {
                                    // ignore: unnecessary_null_comparison
                                    if (value!.isEmpty ||
                                        // ignore: unnecessary_null_comparison
                                        value == null &&
                                            !RegExp(r'^[a-zA-Z]+$')
                                                .hasMatch(value)) {
                                      return 'Please enter a valid book title';
                                    }
                                    return null;
                                  },
                                  initialValue: bookTitle,
                                  onChanged: (val) {
                                    bookTitle = val;
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'Title',
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                                const SizedBox(height: 20,),
                                TextFormField(
                                  validator: (value) {
                                    // ignore: unnecessary_null_comparison
                                    if (value!.isEmpty ||
                                        // ignore: unnecessary_null_comparison
                                        value == null &&
                                            !RegExp(r'^[a-zA-Z]+$')
                                                .hasMatch(value)) {
                                      return 'Please enter a valid book Author';
                                    }
                                    return null;
                                  },
                                  initialValue: bookAuthor,
                                  onChanged: (val) {
                                    bookAuthor = val;
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'Author',
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                                const SizedBox(height: 20,),
                                TextFormField(
                                  validator: (value) {
                                    // ignore: unnecessary_null_comparison
                                    if (value!.isEmpty ||
                                        // ignore: unnecessary_null_comparison
                                        value == null &&
                                            !RegExp(r'^([0-9]{6})$')
                                                .hasMatch(value)) {
                                      return 'Please enter a valid book Code';
                                    }
                                    return null;
                                  },
                                  initialValue: bookCode,
                                  onChanged: (val) {
                                    bookCode = val;
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'Code',
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                                const SizedBox(height: 20,),
                                TextFormField(
                                  validator: (value) {
                                    // ignore: unnecessary_null_comparison
                                    if (value!.isEmpty ||
                                        // ignore: unnecessary_null_comparison
                                        value == null &&
                                            !RegExp(r'^([0-9]{6})$')
                                                .hasMatch(value)) {
                                      return 'Please enter a valid Date';
                                    }
                                    return null;
                                  },
                                  onTap: _handleDate,
                                  readOnly: true,
                                  initialValue: dateOfReturn,
                                  onChanged: (val) {
                                    dateOfReturn = val;
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'Date',
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                                const SizedBox(height: 20,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    ElevatedButton(
                                        onPressed: () {
                                          if (_formKey.currentState!
                                              .validate()) {
                                            updateReturned(
                                                    widget.id,
                                                    bookTitle,
                                                    bookAuthor,
                                                    bookCode,
                                                    user,
                                                    dateOfReturn)
                                                .then((value) => Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                             const ReturnedBookData())));
                                          }
                                        },
                                        child: const Text('Update'))
                                  ],
                                )
                              ],
                            ),
                          );
                        },
                      ))
                ],
              ),
            ),
          ),
        ));
  }
}
