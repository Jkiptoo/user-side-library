import 'package:admin/data/LatestBooksData.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UpdateData extends StatefulWidget {
  final String id;
  const UpdateData({Key? key, required this.id}) : super(key: key);

  @override
  State<UpdateData> createState() => _UpdateDataState();
}

class _UpdateDataState extends State<UpdateData> {
  final _formKey = GlobalKey<FormState>();

  CollectionReference latest = FirebaseFirestore.instance.collection('Latest');

  Future<void> updateLatest(id, title, author, description, code) async {
    return latest
        .doc(id)
        .update({
          'title': title,
          'author': author,
          'description': description,
          'code': code
          // ignore: avoid_print
        })
        // ignore: avoid_print
        .then((value)  {print('User is UPDATED');
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Latest Book Updated Successfully')));
        })
        // ignore: avoid_print, invalid_return_type_for_catch_error
        .catchError((error) {print('Failed to update user $error');
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Latest Book deleted Successfully')));});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        title: const Text('Update Data'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              CircleAvatar(
                radius: 55,
                backgroundColor: Colors.teal[900],
                child: const Icon(
                  Icons.edit,
                  size: 50,
                ),
              ),
              Form(
                  key: _formKey,
                  child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                    future: FirebaseFirestore.instance
                        .collection('Latest')
                        .doc(widget.id)
                        .get(),
                    builder: (_, snapshot) {
                      if (snapshot.hasError) {
                        // ignore: avoid_print
                        print('Something went Wrong');
                      }
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      var data = snapshot.data!.data();
                      var title = data!['title'];
                      var author = data['author'];
                      var code = data['code'];
                      var description = data['description'];
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 20,
                          horizontal: 30,
                        ),
                        child: Column(
                          children: [
                            TextFormField(
                              onChanged: (val) {
                                title = val;
                              },
                              initialValue: title,
                              validator: (value) {
                                // ignore: unnecessary_null_comparison
                                if (value!.isEmpty ||
                                    // ignore: unnecessary_null_comparison
                                    value == null &&
                                        !RegExp(r'^[a-zA-Z]+$')
                                            .hasMatch(value)) {
                                  return 'Please enter a valid book title';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  labelText: 'Book Title',
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10))),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              onChanged: (val) {
                                author = val;
                              },
                              initialValue: author,
                              validator: (value) {
                                if (value!.isEmpty ||
                                    !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                                  return 'Please enter a valid book Author';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  labelText: 'Book Author',
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10))),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              onChanged: (val) {
                                code = val;
                              },
                              initialValue: code,
                              validator: (value) {
                                if (value!.isEmpty ||
                                    !RegExp(r'^([0-9]{6})$')
                                        .hasMatch(value)) {
                                  return 'Enter a valid book Code';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  labelText: 'Book Code',
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10))),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              initialValue: description,
                              onChanged: (val) {
                                description = val;
                              },
                              validator: (value) {
                                if (value!.isEmpty ||
                                    !RegExp(r'^[a-z A-z]').hasMatch(value)) {
                                  return 'Please Enter a valid Description';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  labelText: 'Book Description',
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10))),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: Colors.teal[900]),
                                    onPressed: () {
                                      if (_formKey.currentState!.validate()) {
                                        updateLatest(widget.id, title, author,
                                                description, code)
                                            .then((value) => Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        const LatestBooksData())));
                                      }
                                    },
                                    child: const Text('Update'))
                              ],
                            )
                          ],
                        ),
                      );
                    },
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
