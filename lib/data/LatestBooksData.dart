// ignore_for_file: file_names

import 'package:admin/Services/latest_books.dart';
import 'package:admin/dash/admin_dashboard.dart';
import 'package:admin/data/update_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class LatestBooksData extends StatefulWidget {
  const LatestBooksData({Key? key}) : super(key: key);

  @override
  State<LatestBooksData> createState() => _LatestBooksDataState();
}

class _LatestBooksDataState extends State<LatestBooksData> {

  final Stream<QuerySnapshot> latestStream = FirebaseFirestore.instance.collection('Latest').snapshots();

  CollectionReference latest = FirebaseFirestore.instance.collection('Latest');

  Future<void> deleteLatest(id)async{
    return await latest.doc(id).
    // ignore: avoid_print
    delete().then((value) => print('Book deleted')).catchError((error){
      // ignore: avoid_print
      print('Failed to delete Book: $error');
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AdminDashboard()));
          },
          icon: const Icon(Icons.arrow_back),
        ),
        backgroundColor: Colors.teal[900],
        title: const Text('Latest Books'),
        centerTitle: true,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: latestStream,
        builder: (BuildContext context,AsyncSnapshot<QuerySnapshot> snapshot) {
          if(snapshot.hasError){
            // ignore: avoid_print
            print('Something went wrong');
          }
          if(snapshot.connectionState == ConnectionState.waiting){
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          final List storeDocs = [];
          snapshot.data!.docs.map((DocumentSnapshot documentSnapshot) {
            Map a = documentSnapshot.data() as Map<String, dynamic>;
            storeDocs.add(a);
            a['id'] = documentSnapshot.id;
          }).toList();

          return Container(
            margin: const EdgeInsets.all(2),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Table(
                border: TableBorder.all(),
                columnWidths: const <int, TableColumnWidth>{
                  1: FixedColumnWidth(90),
                },
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                children: [
                  TableRow(
                    children: [
                      TableCell(child:
                      Container(
                        color: Colors.greenAccent,
                        child: const Center(
                          child: Text('Title'),
                        ),
                      )),
                      TableCell(child: Container(
                        color: Colors.greenAccent,
                        child: const Center(child: Text('Author'),),
                      )),
                      TableCell(child: Container(
                        color: Colors.greenAccent,
                        child: const Center(child: Text('Code'),),
                      )),
                      TableCell(child: Container(
                        color: Colors.greenAccent,
                        child: const Center(child: Text('Edit'),),
                      ))
                    ]
                  ),
                  for(var i = 0; i < storeDocs.length; i++)...[
                    TableRow(
                      children: [
                        TableCell(child: Center(
                          child: Text(storeDocs[i]['title'],
                          style: const TextStyle(
                            fontSize: 20
                          ),),
                        )),
                        TableCell(child: Center(
                          child: Text(storeDocs[i]['author'],
                            style: const TextStyle(
                                fontSize: 20
                            ),),
                        )),
                        TableCell(child: Center(
                          child: Text(storeDocs[i]['code'],
                            style: const TextStyle(
                                fontSize: 20
                            ),),
                        )),
                        TableCell(child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            IconButton(
                              onPressed: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>
                                    UpdateData(id: storeDocs[i]['id'])
                                ));
                              },
                              icon: const Icon(Icons.edit_note),
                            ),
                            IconButton(onPressed: (){
                              deleteLatest(storeDocs[i]['id']);
                            }, icon:const Icon(Icons.delete))
                          ],
                        ))
                      ]
                    )
                  ]
                ],
              ),
            )
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>const LatestBook()));
        },
        foregroundColor: Colors.grey[500],
        backgroundColor: Colors.teal[900],
        child: const Icon(
          Icons.add,
          size: 40,
        ),
      ),
    );
  }
}
