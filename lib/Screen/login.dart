// ignore_for_file: must_be_immutable
import 'package:admin/dash/admin_dashboard.dart';
import 'package:admin/reset/password_reset.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  SharedPreferences? adminLogin;
  String? _email;
  String? _password;
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    CheckIfUserIsLoggedIn();
  }
  bool hideText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 100
          ),
          child: Column(
            children: [

              CircleAvatar(
                radius: 105,
                child: ClipOval(
                  child: Image.asset('asset/admin.png'),
                ),
              ),
              const SizedBox(height: 70,),
              Align(
                child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              if (value!.isEmpty ||
                                  !RegExp(r'^[\w-.]+@([\w-]+\.)+[\w-]{2,4}')
                                      .hasMatch(value)) {
                                return 'Enter Valid Email';
                              } else {
                                return null;
                              }
                            },
                            onChanged: (val) {
                              _email = val;
                            },
                            onSaved: (value) {
                              _emailController.text = value!;
                            },
                            decoration: InputDecoration(
                                hintText: 'Email',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10))),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.all(20),
                          child: TextFormField(
                            validator: (value) {
                              String missings = '';
                              if (value!.length < 8) {
                                missings += "Password has at least 8 characters\n";
                              }

                              if (!RegExp("(?=.*[a-z])").hasMatch(value)) {
                                missings +=
                                "Password must contain at least one lowercase letter\n";
                              }
                              if (!RegExp("(?=.*[A-Z])").hasMatch(value)) {
                                missings +=
                                "Password must contain at least one uppercase letter\n";
                              }
                              if (!RegExp((r'\d')).hasMatch(value)) {
                                missings +=
                                "Password must contain at least one digit\n";
                              }
                              if (!RegExp((r'\W')).hasMatch(value)) {
                                missings +=
                                "Password must contain at least one symbol\n";
                              }

                              //if there is password input errors return error string
                              if (missings != "") {
                                return missings;
                              } else {
                                return null;
                              }
                            },
                            obscureText: hideText,
                            controller: _passwordController,
                            onChanged: (val) {
                              setState(() {
                                _password = val;
                              });
                            },
                            onSaved: (value) {
                              _passwordController.text = value!;
                            },
                            decoration: InputDecoration(
                                hintText: 'Password',
                                suffixIcon: GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      hideText = !hideText;
                                    });
                                    FocusScope.of(context).unfocus();
                                  },
                                  child: Icon(
                                    hideText == false?
                                    Icons.visibility:
                                    Icons.visibility_off,
                                    color: Colors.grey[800],
                                  ),
                                ),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10))),
                          ),
                        )
                      ],
                    )),
              ),
              ElevatedButton(
                  onPressed: () async{
                    if (_formKey.currentState!.validate()) {
                      String adminEmail = _emailController.text;
                      if (adminEmail != '') {
                        // ignore: avoid_print
                        print('Successful');
                        adminLogin!.setString('useremail', adminEmail);
                      }
                      await FirebaseAuth.instance
                          .signInWithEmailAndPassword(
                          email: _email!,
                          // ignore: avoid_print
                          password: _password!)
                          .then((value) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AdminDashboard()));
                        ScaffoldMessenger.of(context).showSnackBar( SnackBar(content: Text('Login Successful',style: TextStyle(
                          color: Colors.grey[800],
                          fontSize: 20
                        ),)));
                      }).catchError((err){
                        showDialog(context: context, builder: (BuildContext context){
                          return AlertDialog(
                            title: const Text('Error'),
                            content: Text('$err'),
                            actions: [
                              ElevatedButton(onPressed: (){
                                Navigator.of(context).pop();
                                ScaffoldMessenger.of(context).showSnackBar( SnackBar(content: Text('Please check your email and password',style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 20
                                ),)));
                              }, child: const Text('Ok'))
                            ],
                          );
                        });
                      });
                      adminLogin!.setString('adminEmail', adminEmail);
                    }
                  },
                  child: const Text('LOGIN')),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>const PasswordReset()));
                  }, child:const Text('Password Reset')),
                  const SizedBox(width: 20,)
                ],
              )

            ],
          ),
        ),
      ),
    );
  }

  // ignore: non_constant_identifier_names
  void CheckIfUserIsLoggedIn() async {
    adminLogin = await SharedPreferences.getInstance();
  }
}
