import 'package:admin/collection/return_data_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';

class ReturnBook extends StatefulWidget {
  const ReturnBook({
    Key? key,
  }) : super(key: key);

  @override
  State<ReturnBook> createState() => _ReturnBookState();
}

class _ReturnBookState extends State<ReturnBook> {
  final DateTime _time = DateTime.now();
  final DateFormat _dateFormatter = DateFormat('MMM dd yyyy');

  _handleDatePicker() async {
    final DateTime? date = await showDatePicker(
        context: context,
        initialDate: _time,
        firstDate: DateTime(2022),
        lastDate: DateTime(2040));
    if (date != null && date != _time) {
      setState(() {
        date;
      });
      _dateController.text = _dateFormatter.format(date);
    }
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  var user = '';
  var title = '';
  var author = '';
  var code = '';
  var date = '';
  final _userController = TextEditingController();
  final _titleController = TextEditingController();
  final _authorController = TextEditingController();
  final _bookCodeController = TextEditingController();
  final _dateController = TextEditingController();

  clearText() {
    _userController.clear();
    _titleController.clear();
    _authorController.clear();
    _bookCodeController.clear();
    _dateController.clear();
  }

  CollectionReference bookReturned =
  FirebaseFirestore.instance.collection('BookData');

  Future<void> returnBook() async {
    return await bookReturned
        .add({
      'user': user,
      'bookTitle': title,
      'bookAuthor': author,
      'dateOfReturn': date,
      'bookCode': code,
    }
      // ignore: avoid_print
    )
    // ignore: avoid_print
        .then((value) => print('Returned data added successfully'))
        .catchError((error) {
      // ignore: avoid_print
      print('Something went wrong: $error');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.teal[900],
        title: const Text('Return Book'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Container(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CircleAvatar(
                    backgroundImage: AssetImage('asset/returned.png'),
                    radius: 100,
                    child: ClipOval(),
                  )
                ],
              ),
            ),
            Form(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        autofocus: false,
                        controller: _userController,
                        validator: (value) {
                          if (value!.isEmpty ||
                              !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                            return 'Enter a valid user';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'User',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        autofocus: false,
                        validator: (value) {
                          if (value!.isEmpty ||
                              !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                            return 'Enter a Valid Book Title';
                          }
                          return null;
                        },
                        controller: _titleController,
                        decoration: InputDecoration(
                            labelText: 'Book Title',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        autofocus: false,
                        validator: (value) {
                          if (value!.isEmpty ||
                              !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                            return 'Enter valid Book Author';
                          }
                          return null;
                        },
                        controller: _authorController,
                        decoration: InputDecoration(
                            labelText: 'Book Author',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        autofocus: false,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Date is Needed';
                          }
                          return null;
                        },
                        controller: _bookCodeController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: 'Book Code',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: TextFormField(
                        autofocus: false,
                        readOnly: true,
                        onTap: _handleDatePicker,
                        controller: _dateController,
                        decoration: InputDecoration(
                            labelText: 'Date of return',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                backgroundColor: Colors.teal),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                setState(() {
                                  user = _userController.text;
                                  author = _authorController.text;
                                  title = _titleController.text;
                                  code = _bookCodeController.text;
                                  date = _dateController.text;
                                  returnBook().then((value) =>
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                              const ReturnedBookData())));
                                  clearText();
                                });
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text('Successful')));
                              }
                            },
                            child: const Text(
                              'Return Book',
                              style: TextStyle(fontSize: 20),
                            ))
                      ],
                    ),
                  ],
                ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal[900],
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const ReturnedBookData()));
        },
        child: const Icon(
          Icons.edit_note,
          size: 40,
        ),
      ),
    );
  }
}