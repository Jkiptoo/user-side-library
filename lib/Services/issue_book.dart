import 'package:admin/data/issue_book_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';
import '../dash/admin_dashboard.dart';

class IssueBook extends StatefulWidget {
  const IssueBook({Key? key}) : super(key: key);

  @override
  State<IssueBook> createState() => _IssueBookState();
}

class _IssueBookState extends State<IssueBook> {
  var user = '';
  var dateOfIssue = '';
  var dateOfReturn = '';
  var title = '';
  var author = '';
  var bookCode = '';
  CollectionReference issueBook =
      FirebaseFirestore.instance.collection('BooksOut');
  Future<void> issuedBooks() async {
    issueBook
        .add({
          'user': user,
          'dateOfIssue': dateOfIssue,
          'dateOfReturn': dateOfReturn,
          'title': title,
          'author': author,
          'bookCode': bookCode,
        })
        .then((value) => ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Issued book added'))))
        .catchError((error) {
          ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Something Went Wrong')));
        });
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final DateTime _date = DateTime.now();
  final TextEditingController _userController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _authorController = TextEditingController();
  final TextEditingController _codeController = TextEditingController();
  final TextEditingController _statusController = TextEditingController();
  final _returnController = TextEditingController();
  final _issueController = TextEditingController();
  final DateFormat _dateFormatter = DateFormat('MMM dd yyyy');
 //  final differenceInDays = dateTimeNow.difference().inDays;
 // print('$differenceInDays');
  _handleDatePicker() async {
    final DateTime? date = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2022),
      lastDate: DateTime(2050),
    );
    if (date != null && date != _date) {
      setState(() {
        date;
      });
      _returnController.text = _dateFormatter.format(date);
    }
  }
  // DateTime date1 = DateTime.now();
  // DateTime date2 = DateTime.now().add(const Duration(days: 1));
  // Future<int> getDifference(DateTime date1,DateTime date2) async {
  //   print('date1 $date1');
  //   print('date2 $date2');
  //   print("${date1.difference(date2).inHours}");
  //   return date1.difference(date2).inHours;
  // }
  final DateFormat _dateFormat = DateFormat('MMM dd yyyy');
  _handleDate() async {
    final DateTime? dates = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (dates != null) {
      setState(() {
        dates;
      });
      _issueController.text = _dateFormat.format(dates);
    }
  }

  void clearText() {
    _userController.clear();
    _issueController.clear();
    _titleController.clear();
    _authorController.clear();
    _codeController.clear();
    _statusController.clear();
    _returnController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          child: const Icon(Icons.arrow_back),
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AdminDashboard()));
          },
        ),
        backgroundColor: Colors.teal[900],
        centerTitle: true,
        title: const Text(
          'ISSUE\n BOOK',
          style: TextStyle(color: Colors.white),
          textAlign: TextAlign.center,
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Text(
                      'User',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'User is needed';
                        }
                        return null;
                      },
                      controller: _userController,
                      decoration: const InputDecoration(hintText: ''),
                      onSaved: (value) => setState(() {
                        _userController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Date of Issue',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Date is Needed';
                        }
                        return null;
                      },
                      readOnly: true,
                      controller: _issueController,
                      onTap: _handleDate,
                      decoration: const InputDecoration(),
                      onSaved: (value) => setState(() {
                        _issueController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Date Of Return',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Flexible(
                        child: TextFormField(
                      readOnly: true,
                      controller: _returnController,
                      onTap: _handleDatePicker,
                      decoration: const InputDecoration(),
                      onSaved: (value) {
                        setState(() {
                          _returnController.text = value!;
                        });
                      },
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Title',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Title is needed';
                        }
                        return null;
                      },
                      controller: _titleController,
                      decoration: const InputDecoration(hintText: ''),
                      onSaved: (value) => setState(() {
                        _titleController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Author',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Author is needed';
                        }
                        return null;
                      },
                      controller: _authorController,
                      decoration: const InputDecoration(hintText: ''),
                      onSaved: (value) => setState(() {
                        _authorController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text(
                      'Book Code',
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                    const SizedBox(
                      width: 25,
                    ),
                    Flexible(
                        child: TextFormField(
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Book Code is needed';
                        }
                        return null;
                      },
                      controller: _codeController,
                      decoration: const InputDecoration(hintText: ''),
                      onSaved: (value) => setState(() {
                        _codeController.text = value!;
                      }),
                    ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.teal[900],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15))),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            user = _userController.text;
                            dateOfIssue = _issueController.text;
                            dateOfReturn = _returnController.text;
                            title = _titleController.text;
                            author = _authorController.text;
                            bookCode = _codeController.text;
                            issuedBooks();
                            clearText();
                          });
                        }
                      },
                      child: const Text(
                        'ISSUE',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal[900],
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>const IssueBookPage()));
        },
        child: const Icon(Icons.edit_note, size: 40),
      ),
    );
  }
}
