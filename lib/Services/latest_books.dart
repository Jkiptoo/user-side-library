import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import '../dash/admin_dashboard.dart';
import '../data/LatestBooksData.dart';

class LatestBook extends StatefulWidget {
  const LatestBook({Key? key}) : super(key: key);

  @override
  State<LatestBook> createState() => _LatestBookState();
}

class _LatestBookState extends State<LatestBook> {
  var title = '';
  var author = '';
  var code = '';
  var description = '';
  final _formKey = GlobalKey<FormState>();
  final _titleController = TextEditingController();
  final _authorController = TextEditingController();
  final _codeController = TextEditingController();
  final _descriptionController = TextEditingController();

  void clearText() {
    _titleController.clear();
    _authorController.clear();
    _codeController.clear();
    _descriptionController.clear();
  }

  CollectionReference book = FirebaseFirestore.instance.collection('Latest');
  Future<void> addLatest() async {
    return book.add({
      'title': title,
      'author': author,
      'code': code,
      // ignore: avoid_print
      'description': description
    })
        // ignore: avoid_print
        .then((value) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Latest book added')));
    }).catchError((error) {
      // ignore: avoid_print
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                'Error Message',
                style: TextStyle(
                    color: Colors.grey[800],
                    fontSize: 30,
                    fontWeight: FontWeight.bold),
              ),
              content: Text(
                '$error',
                style: TextStyle(color: Colors.grey[700]),
              ),
              actions: [
                ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'OK',
                      style: TextStyle(color: Colors.grey[800]),
                    ))
              ],
            );
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[900],
        leading: IconButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AdminDashboard()));
          },
          icon: const Icon(
            Icons.arrow_back,
            size: 30,
          ),
        ),
        title: const Text('Latest Books'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  autofocus: false,
                  onSaved: (value) {
                    _titleController.text = value!;
                  },
                  controller: _titleController,
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                      return 'Enter Valid Book Title';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0)),
                    labelText: 'Book Title',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  autofocus: false,
                  onSaved: (value) {
                    _authorController.text = value!;
                  },
                  controller: _authorController,
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                      return 'Enter valid Author';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0)),
                    labelText: 'Book Author',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  autofocus: false,
                  onSaved: (value) {
                    _codeController.text = value!;
                  },
                  controller: _codeController,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Enter Valid Book Code';
                    }
                    if (value.length <= 5) {
                      return 'Book Code Requires at least five Characters';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0)),
                    labelText: 'Book Code',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  autofocus: false,
                  controller: _descriptionController,
                  onSaved: (value) {
                    _descriptionController.text = value!;
                  },
                  maxLines: 5,
                  validator: (value) {
                    if (value!.isEmpty ||
                        !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                      return 'Enter a valid Book Description';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0)),
                    labelText: 'Book Description',
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.teal[900],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5))),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        setState(() {
                          title = _titleController.text;
                          author = _authorController.text;
                          code = _codeController.text;
                          description = _descriptionController.text;
                          addLatest();
                          clearText();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      const LatestBooksData()));
                        });
                      }
                    },
                    child: const Text(
                      'Submit',
                      style: TextStyle(fontSize: 20),
                    )),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal[800],
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const LatestBooksData()));
        },
        child: Icon(
          Icons.edit_note,
          color: Colors.grey[200],
          size: 40,
        ),
      ),
    );
  }
}
