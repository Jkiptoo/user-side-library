import 'dart:io';
import 'package:admin/data/add_book_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class AddBook extends StatefulWidget {
  const AddBook({Key? key}) : super(key: key);

  @override
  State<AddBook> createState() => _AddBookState();
}

class _AddBookState extends State<AddBook> {
  File? _image;
  final imagePicker = ImagePicker();
  String? downloadUrl;
  Future imagePickerMethod() async {
    // ignore: invalid_use_of_visible_for_testing_member
    final pick =
        // ignore: invalid_use_of_visible_for_testing_member
        await ImagePicker.platform.getImage(source: ImageSource.gallery);
    setState(() {
      if (pick != null) {
        _image = File(pick.path);
      } else {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('No File Selected')));
      }
    });
  }

  Future uploadImage() async {
    Reference reference = FirebaseStorage.instance.ref().child('images');
    await reference.putFile(_image!);
    downloadUrl = await reference.getDownloadURL();
    // ignore: avoid_print
    print(downloadUrl);
  } // ignore: prefer_typing_uninitialized_variables

  // ignore: prefer_typing_uninitialized_variables
  var selectedType;
  final List<String> _categoryList = <String>[
    'Programming',
    'Fictional',
    'Non-Fictional',
    'Documentary',
    'Religious',
    'Fantasy',
    'Poetry',
    'Biography',
    'Novel',
    'Historical'
  ];
  final database = FirebaseFirestore.instance;
  uploadData(String title) async {
    List<String> splitList = title.split(' ');
    List<String> indexList = [];

    for (int i = 0; i < splitList.length; i++) {
      for (int j = 0; j < splitList[i].length + i; j++) {
        indexList.add(splitList[i].substring(0, j).toLowerCase());
      }
    }
    database
        .collection('bookCollection')
        .add({'title': title, 'searchIndex': indexList});
  }

  final _formKey = GlobalKey<FormState>();
  var book = '';
  var bookAuthor = '';
  var bookDescription = '';
  var category = '';
  final _titleController = TextEditingController();
  final _authorController = TextEditingController();
  final _descriptionController = TextEditingController();
  clearText() async {
    _titleController.clear();
    _authorController.clear();
    _descriptionController.clear();
  }

  CollectionReference bookReference =
      FirebaseFirestore.instance.collection('books');
  Future<void> bookStore() async {
    bookReference
        .add({
          'author': bookAuthor,
          'category': selectedType.toString(),
          'title': book,
          'description': bookDescription,
        }
            // ignore: avoid_print
            )
        .then((value) => ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Book Added Successfully'))))
        .catchError((error) {
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text('Something went Wrong: $error')));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Book'),
        centerTitle: true,
        backgroundColor: Colors.teal[900],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Form(
          key: _formKey,
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(0),
                  child: Row(
                    children: [
                      const Text('Book Title'),
                      const SizedBox(
                        width: 30,
                      ),
                      Flexible(
                          child: TextFormField(
                        validator: (value) {
                          if (value == null ||
                              !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                            return 'Enter a valid Book Title';
                          }
                          return null;
                        },
                        autofocus: false,
                        controller: _titleController,
                        decoration: const InputDecoration(
                          labelText: 'Enter Book Title',
                        ),
                      ))
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: const EdgeInsets.all(0),
                  child: Row(
                    children: [
                      const Text('Book Author'),
                      const SizedBox(
                        width: 20,
                      ),
                      Flexible(
                          child: TextFormField(
                        validator: (value) {
                          if (value == null ||
                              !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                            return 'Enter a valid Book Author';
                          }
                          return null;
                        },
                        autofocus: false,
                        controller: _authorController,
                        decoration: const InputDecoration(
                            hintText: 'Enter Book Author'),
                      ))
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: const EdgeInsets.all(0),
                  child: Row(
                    children: [
                      const Text('Category'),
                      const SizedBox(
                        width: 30,
                      ),
                      Flexible(
                        child: DropdownButton(
                          items: _categoryList
                              .map((value) => DropdownMenuItem(
                                  value: value, child: Text(value)))
                              .toList(),
                          onChanged: (selectedCategoryType) {
                            setState(() {
                              selectedType = selectedCategoryType;
                            });
                          },
                          value: selectedType,
                          isExpanded: true,
                          hint: const Text('Select Book Category'),
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding: const EdgeInsets.all(0),
                  child: Row(
                    children: [
                      const Text('Description'),
                      const SizedBox(
                        width: 20,
                      ),
                      Flexible(
                          child: TextFormField(
                        validator: (value) {
                          if (value == null ||
                              !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                            return 'Enter a valid Book Description';
                          }
                          return null;
                        },
                        controller: _descriptionController,
                        autofocus: false,
                        maxLines: 7,
                        decoration: InputDecoration(
                            hintText: 'Enter Book Description',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0))),
                      ))
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Stack(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const SizedBox(
                          width: 30,
                        ),
                        CircleAvatar(
                            backgroundColor: Colors.white,
                            radius: 70,
                            child: ClipOval(
                              child: _image == null
                                  ? const Center(
                                      child: Text('No Image Selected'),
                                    )
                                  : Image.file(
                                      _image!,
                                      fit: BoxFit.fill,
                                      width: 150,
                                      matchTextDirection: true,
                                    ),
                            )),
                        Padding(
                          padding: const EdgeInsets.only(top: 90),
                          child: IconButton(
                              onPressed: () {
                                imagePickerMethod();
                              },
                              icon: const Icon(
                                Icons.camera_alt_outlined,
                                size: 40,
                              )),
                        ),
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.teal[900],
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10))),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            setState(() {
                              book = _titleController.text;
                              bookAuthor = _authorController.text;
                              bookDescription = _descriptionController.text;
                              category = selectedType.toString();
                              uploadData(_titleController.text);
                              bookStore().then((value) => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const FetchBooksData())));
                              clearText();
                            });
                          }
                        },
                        child: const Text('SUBMIT'))
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal[900],
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const FetchBooksData()));
        },
        child: const Icon(
          Icons.edit_note,
          size: 40,
        ),
      ),
    );
  }
}
