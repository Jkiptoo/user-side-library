import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class RetrieveLostBooks extends StatefulWidget {
  const RetrieveLostBooks({Key? key}) : super(key: key);

  @override
  State<RetrieveLostBooks> createState() => _RetrieveLostBooksState();
}

class _RetrieveLostBooksState extends State<RetrieveLostBooks> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.teal[900],
        title: Text('Lost book',style: TextStyle(
          color: Colors.grey[300],
          fontSize: 25,
          fontWeight: FontWeight.bold
        ),),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('Lost').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            const Center(
              child: CircularProgressIndicator(),
            );
          }
          final List storedBooks = [];
          snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
            Map l = documentSnapshot.data() as Map<String, dynamic>;
            storedBooks.add(l);
            l['id']= documentSnapshot.id;
          }).toList();
          return ListView(
            children: [
              for(var i=0; i<storedBooks.length; i++)...[
                ListTile(
                  title: Text(storedBooks[i]['bookTitle']),
                  trailing: Text(storedBooks[i]['bookCode']),
                  subtitle: Text(storedBooks[i]['user']),
                )
              ]
            ],
          );
        },
      )
    );
  }
}
