import 'package:admin/Services/latest_books.dart';
import 'package:admin/dash/admin_dashboard.dart';
import 'package:admin/splash/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const SplashScreen(),
      routes: {
        '/dash/': (context) => AdminDashboard(),
        '/latest/': (context) => const LatestBook(),
      },
    );
  }
}
